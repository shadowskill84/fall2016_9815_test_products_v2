# README #

### Class ###
* MATH 9815: Software Engineering for Finance
* SOA & the ProductService (Revisited)

### Authors ###
* Thomas,Christopher

### Script Summary ###
Test new implementation of Service Oriented Architecture. 

### Usage ###
Program built with Visual Studio Express 2013. To load solution in Visual Studio simply open "fall2016_9815_cpp_hw02_soa_service.sln".  
To run with g++, change to directory with _test.cpp_ and execute the following command line: **g++ -I <boost/path> *.cpp --std=c++11 -pthread -lrt -lboost_date_time**  
You may need to install additional libraries, such as datetime package with **sudo apt-get install libboost-date-time-dev**  

### Libraries needed: ###
* iostream
* string
* map
* boost/shared_ptr.hpp
* boost/date_time/gregorian/gregorian.hpp
* boost/interprocess/managed_shared_memory.hpp