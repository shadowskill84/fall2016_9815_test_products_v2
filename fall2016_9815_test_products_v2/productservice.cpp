/**
 * productservice.hpp defines Bond and IRSwap ProductServices
 */

#include "productservice.hpp"

using std::pair;

BondProductService::BondProductService()
{
  bondMap = map<string,BondPtr>();
}

BondPtr BondProductService::GetData(string productId)
{
  return bondMap[productId];
}

void BondProductService::Add(BondPtr bond)
{
	bondMap.insert(pair<string, BondPtr>(bond->GetProductId(), bond));
}

// Get all Bonds with the specified ticker
vector<BondPtr> BondProductService::GetBonds(const string& ticker)
{
	//vector of bonds to return
	vector<BondPtr> bondVect(0);

	//iterate over bond map, adding any bonds with matching ticker
	for (map<string, BondPtr>::iterator it = bondMap.begin(); it != bondMap.end(); ++it)
		if (it->second->GetTicker() == ticker) bondVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return bondVect;
}

IRSwapProductService::IRSwapProductService()
{
	swapMap = map<string, IRSwapPtr>();
}

IRSwapPtr IRSwapProductService::GetData(string productId)
{
  return swapMap[productId];
}

void IRSwapProductService::Add(IRSwapPtr swap)
{
	swapMap.insert(pair<string, IRSwapPtr>(swap->GetProductId(), swap));
}

// Get all Swaps with the specified fixed leg day count convention
vector<IRSwapPtr> IRSwapProductService::GetSwaps(DayCountConvention _fixedLegDayCountConvention)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetFixedLegDayCountConvention() == _fixedLegDayCountConvention) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with the specified fixed leg payment frequency
vector<IRSwapPtr> IRSwapProductService::GetSwaps(PaymentFrequency _fixedLegPaymentFrequency)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetFixedLegPaymentFrequency() == _fixedLegPaymentFrequency) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with the specified floating index
vector<IRSwapPtr> IRSwapProductService::GetSwaps(FloatingIndex _floatingIndex)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetFloatingIndex() == _floatingIndex) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with a term in years equal to the specified value
vector<IRSwapPtr> IRSwapProductService::GetSwaps(int _termYears)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetTermYears() == _termYears) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with a term in years greater than the specified value
vector<IRSwapPtr> IRSwapProductService::GetSwapsGreaterThan(int _termYears)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetTermYears() > _termYears) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with a term in years less than the specified value
vector<IRSwapPtr> IRSwapProductService::GetSwapsLessThan(int _termYears)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetTermYears() < _termYears) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with the specified swap type
vector<IRSwapPtr> IRSwapProductService::GetSwaps(SwapType _swapType)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetSwapType() == _swapType) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}

// Get all Swaps with the specified swap leg type
vector<IRSwapPtr> IRSwapProductService::GetSwaps(SwapLegType _swapLegType)
{
	//vector of IRSwaps to return
	vector<IRSwapPtr> swapVect(0);

	//iterate over irswap map, adding any swaps with matching parameter
	for (map<string, IRSwapPtr>::iterator it = swapMap.begin(); it != swapMap.end(); ++it)
		if (it->second->GetSwapLegType() == _swapLegType) swapVect.push_back(it->second);

	//return vector of bonds now containing all pertinent bonds
	return swapVect;
}
