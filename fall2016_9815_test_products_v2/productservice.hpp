/**
 * productservice.hpp defines Bond and IRSwap ProductServices
 */

#ifndef productservice_hpp
#define productservice_hpp

#include <map>
#include "products.hpp"
#include "soa.hpp"

using std::map;
using std::vector;

typedef boost::shared_ptr<Bond> BondPtr;
typedef boost::shared_ptr<IRSwap> IRSwapPtr;

/**
 * Bond Product Service to own reference data over a set of bond securities.
 * Key is the productId string, value is a Bond.
 */
class BondProductService : public Service<string, BondPtr>
{

public:
  // BondProductService ctor
  BondProductService();

  // Return the bond data for a particular bond product identifier
  BondPtr GetData(string productId);

  // Add a bond to the service (convenience method)
  void Add(BondPtr bond);

  // Get all Bonds with the specified ticker
  vector<BondPtr> GetBonds(const string& ticker);

private:
	map<string, BondPtr> bondMap; // cache of bond products

};

/**
 * Interest Rate Swap Product Service to own reference data over a set of IR Swap products
 * Key is the productId string, value is a IRSwap.
 */
class IRSwapProductService : public Service<string, IRSwapPtr>
{
public:
  // IRSwapProductService ctor
  IRSwapProductService();

  // Return the IR Swap data for a particular bond product identifier
  IRSwapPtr GetData(string productId);

  // Add a bond to the service (convenience method)
  void Add(IRSwapPtr swap);

  // Get all Swaps with the specified fixed leg day count convention
  vector<IRSwapPtr> GetSwaps(DayCountConvention _fixedLegDayCountConvention);

  // Get all Swaps with the specified fixed leg payment frequency
  vector<IRSwapPtr> GetSwaps(PaymentFrequency _fixedLegPaymentFrequency);

  // Get all Swaps with the specified floating index
  vector<IRSwapPtr> GetSwaps(FloatingIndex _floatingIndex);

  // Get all Swaps with a term in years equal to the specified value
  vector<IRSwapPtr> GetSwaps(int _termYears);
  
  // Get all Swaps with a term in years greater than the specified value
  vector<IRSwapPtr> GetSwapsGreaterThan(int _termYears);

  // Get all Swaps with a term in years less than the specified value
  vector<IRSwapPtr> GetSwapsLessThan(int _termYears);

  // Get all Swaps with the specified swap type
  vector<IRSwapPtr> GetSwaps(SwapType _swapType);

  // Get all Swaps with the specified swap leg type
  vector<IRSwapPtr> GetSwaps(SwapLegType _swapLegType);

private:
	map<string, IRSwapPtr> swapMap; // cache of IR Swap products

};

#endif
